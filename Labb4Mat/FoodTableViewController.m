//
//  FoodTableViewController.m
//  Labb4Mat
//
//  Created by Viktor Jegerås on 2015-02-23.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "FoodTableViewController.h"
#import "DetailViewController.h"
#import "food.h"
#import "MyTableViewCell.h"
@interface FoodTableViewController ()

@property (nonatomic) NSArray *searchResult;
@property (nonatomic) NSArray *data;
@property (nonatomic) NSDictionary *nutrients;
@property (nonatomic) NSDictionary *nutDict;
@property (nonatomic) NSString *tempString;
@property (nonatomic) NSString *tempFoodName;
@property (nonatomic) NSString *tempHealthy;
@end

@implementation FoodTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tempString = @"";
    self.tempFoodName = @"";
    self.tempHealthy = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        return self.allFood.count;
    }else{
        return self.searchResult.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];

    if(tableView == self.tableView){
        self.data = self.allFood;
        
    }else{
        self.data = self.searchResult;
    }
    
    cell.foodID = [self.data[indexPath.row] number];
    cell.foodIDText.text = [NSString stringWithFormat:@"Number: %@",cell.foodID];
    cell.foodName.text = [self.data[indexPath.row] name];
    
    return cell;
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText; {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    self.searchResult = [self.allFood filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.allFood removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    MyTableViewCell *cell = (MyTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    self.tempFoodName = cell.foodName.text;
    
    [self LoadApiNr:cell.foodID withCell:cell];
    
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DetailViewController *detailView = [segue destinationViewController];
    
    detailView.detailText = self.tempString;
    detailView.title = self.tempFoodName;
    detailView.healthyText = self.tempHealthy;
    
}

-(void)LoadApiNr:(NSNumber*)number withCell:(MyTableViewCell*)cell{
    NSString* url = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@",number ];
    NSURL *matApi = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:matApi];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *parseError;
        self.nutrients = [NSJSONSerialization JSONObjectWithData:data
                                                     options:kNilOptions error:&parseError];
        
        self.nutDict = self.nutrients[@"nutrientValues"];
        
        for(NSDictionary *key in self.nutDict){
            self.tempString = [self.tempString stringByAppendingString: [NSString stringWithFormat:@"%@ = %@ \n", key, [self.nutDict objectForKey:key]]];
        }
        
        cell.fat = [self.nutDict objectForKey:@"fat"];
        cell.fiber = [self.nutDict objectForKey:@"fibres"];
        cell.protein = [self.nutDict objectForKey:@"protein"];
        
        if((cell.protein.doubleValue + cell.fiber.doubleValue) >= cell.fat.doubleValue){
            self.tempHealthy = @"Healthy";
        }else{
            self.tempHealthy = @"NOT Healthy!!";
        }
        
        NSLog(@"fat: %@ fiber: %@ protein: %@",cell.fat, cell.fiber, cell.protein);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"detail" sender:self];
        });
    }];
    [task resume];
}

@end
