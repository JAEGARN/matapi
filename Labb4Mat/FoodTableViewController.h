//
//  FoodTableViewController.h
//  Labb4Mat
//
//  Created by Viktor Jegerås on 2015-02-23.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodTableViewController : UITableViewController <UISearchBarDelegate>
@property (nonatomic) NSMutableArray* allFood;
@end
