//
//  DetailViewController.h
//  Labb4Mat
//
//  Created by Viktor Jegerås on 2015-02-23.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *detailTextField;
@property (weak, nonatomic) IBOutlet UILabel *healthyLabel;
@property (nonatomic) NSString *detailText;
@property (nonatomic) NSString *healthyText;
@end
