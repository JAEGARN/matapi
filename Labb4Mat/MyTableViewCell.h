//
//  MyTableViewCell.h
//  Labb4Mat
//
//  Created by Viktor Jegerås on 2015-03-09.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell

@property (nonatomic) NSString *tempFoodName;
@property (weak, nonatomic) IBOutlet UILabel *foodName;
@property (nonatomic) NSNumber *foodID;
@property (weak, nonatomic) IBOutlet UILabel *foodIDText;
@property (nonatomic) NSNumber *fiber;
@property (nonatomic) NSNumber *fat;
@property (nonatomic) NSNumber *protein;
@end
