//
//  food.h
//  Labb4Mat
//
//  Created by Viktor Jegerås on 2015-02-25.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface food : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber *number;
@property (nonatomic) NSString *nutrient;
- (instancetype)initWithName:(NSString*)name AndNumber:(NSNumber*)id AndNutrient:(NSString*)nut;
@end

