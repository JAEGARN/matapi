//
//  food.m
//  Labb4Mat
//
//  Created by Viktor Jegerås on 2015-02-25.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "food.h"

@implementation food

- (instancetype)initWithName:(NSString*)name AndNumber:(NSNumber*)id AndNutrient:(NSString*)nut
{
    self = [super init];
    if (self) {
        self.nutrient = nut;
        self.number = id;
        self.name = name;
    }
    return self;
}

@end
