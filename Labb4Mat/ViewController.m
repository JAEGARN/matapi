//
//  ViewController.m
//  Labb4Mat
//
//  Created by Viktor Jegerås on 2015-02-16.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UIButton *loadButton;

@property (nonatomic) NSTimer *timer;
@property (nonatomic) int counter;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] init];
    
    self.image.hidden = YES;
    self.loadButton.hidden = YES;
    self.counter = 0;
    
    [self startTimer];
}

-(void)startTimer {
    self.timer = [NSTimer
                  scheduledTimerWithTimeInterval:0.2
                  target:self
                  selector:@selector(startUpAni)
                  userInfo:nil repeats:YES];
}

- (void) stopTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)loadApi:(id)sender {
    }

-(void)startUpAni{
    self.counter++;
    if(self.counter < 30){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(arc4random() %(int)(self.view.frame.size.width - 60), self.view.frame.origin.y - 45, 60, 60)];
        label.text = @"Mat!";
        label.textColor = [UIColor colorWithRed:arc4random() %(3) green:arc4random() %(3) blue:arc4random() %(3) alpha:1];
        
        [self.view addSubview:label];
        [self.gravity addItem:label];
    
        [self.animator addBehavior:self.gravity];
    }else{
        [self stopTimer];
        self.image.hidden = NO;
        self.loadButton.hidden = NO;
    }
    
}

@end
