//
//  DetailViewController.m
//  Labb4Mat
//
//  Created by Viktor Jegerås on 2015-02-23.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewDidLayoutSubviews{
    if([self.healthyText  isEqual: @"NOT Healthy!!"]){
        self.healthyLabel.textColor = [UIColor redColor];
    }else{
        self.healthyLabel.textColor = [UIColor greenColor];
    }
    self.detailTextField.text = self.detailText;
    self.healthyLabel.text = self.healthyText;
}

- (void)viewDidAppear:(BOOL)animated {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
