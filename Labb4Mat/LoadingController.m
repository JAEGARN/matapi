//
//  LoadingController.m
//  Labb4Mat
//
//  Created by Viktor Jegerås on 2015-02-23.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "LoadingController.h"

@interface LoadingController ()
@property NSDictionary *foods;
@property NSMutableArray* theFoods;
@property (weak, nonatomic) IBOutlet UILabel *loadLabel;
@end

@implementation LoadingController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.foods = [[NSDictionary alloc] init];
    self.theFoods = [[NSMutableArray alloc] init];
    
    [self LoadAPI];
    
}

- (void)viewDidLayoutSubviews{
    [self loading];
}

-(void) LoadAPI {
    NSURL *matApi = [NSURL URLWithString:@"http://matapi.se/foodstuff"];
    NSURLRequest *request = [NSURLRequest requestWithURL:matApi];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler: ^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *parseError;
        self.foods = [NSJSONSerialization JSONObjectWithData:data
                                options:kNilOptions error:&parseError];
        for(NSDictionary *key in self.foods){
            //NSLog(@"name %@ number %@ nutrinet %@", key[@"name"], key[@"number"], key[@"nutrientValues"]);
            [self.theFoods addObject:[[food alloc] initWithName:key[@"name"] AndNumber:key[@"number"] AndNutrient:key[@"nutrientValues"]]];
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self performSegueWithIdentifier: @"showTableViewSegue" sender:self];
        });
    }];
    
    [task resume];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    FoodTableViewController * foodTable = [segue destinationViewController];
    foodTable.allFood = self.theFoods;
}

- (void) loading {
    CGPoint origin = CGPointMake(0 + self.loadLabel.frame.size.width / 2, self.view.frame.size.height / 2);
    CGPoint endPos = CGPointMake(self.view.frame.size.width - self.loadLabel.frame.size.width / 2 , self.view.frame.size.height / 2);
    
    [UIView animateWithDuration:1.0 animations:^{
        self.loadLabel.center = endPos;
    } completion:^(BOOL finished){
        [UIView animateWithDuration:1.0 delay:0.0 options:kNilOptions animations:^{
            self.loadLabel.center = origin;
        }completion:^(BOOL finished){
            [self loading];
        }];

    }];

}

@end
